import * as request from 'supertest'
import { Test } from '@nestjs/testing'
import { AppModule } from './../src/app.module'
import { INestApplication } from '@nestjs/common'

describe('AppController (e2e)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule]
    }).compile()

    app = moduleFixture.createNestApplication()
    await app.init()
  })

  it('/ (POST) should works fine', async () => {
    return await request(app.getHttpServer())
      .post('/')
      .send({ url: 'https://www.fasterize.com/fr/' })
      .expect(201)
      .expect({
        plugged: true,
        statusCode: 200,
        fstrzFlags: ['optimisée', 'cachée'],
        cloudfrontStatus: 'MISS',
        cloudfrontPOP: 'Paris'
      })
  })

  it('/ (POST) should works fine', async () => {
    return await request(app.getHttpServer())
      .post('/')
      .send({ url: 'asterize.com/fr/' })
      .expect(403)
      .expect({ statusCode: 403, message: 'invalid url provided !' })
  })
})
