import { Injectable } from '@nestjs/common'
import { AppError } from './domain/exceptions/app.error'
import axios from 'axios'
import { CFData } from './types/Locations'
import { fasterizeHeaders } from './data/fasterize/fasterizeHeaders'
import { GetHeadersResult } from './types/headers'
import { CheckUrlResponse } from './domain/DTO/CheckUrlResponseDTO'
import { isValidURL } from './utils'
import { GenerateCheckUrlBody } from './types/app'

@Injectable()
export class AppService {
  async getHeaders (url: string): Promise<GetHeadersResult> {
    try {
      const { headers, status } = await axios.get(url)
      return { headers, status }
    } catch (error) {
      throw new AppError(500, error)
    }
  }

  async getCFLocation (location: string): Promise<string> {
    const { data }: { data: CFData} = await axios.get('https://www.cloudping.cloud/cloudfront-edge-locations.json')
    const cfLocations = Object.keys(data.nodes)
    if (!cfLocations.includes(location.substring(0, 3))) return ''
    return data.nodes[location.substring(0, 3)].city
  }

  getCFStatus (CFStatus: string): string {
    if (CFStatus === null) throw new AppError(403, 'CloufrontPop header invalid')
    return CFStatus.split(' ')[0].toUpperCase()
  }

  getFasterizeHeadersMeaning (fstrzFlags: string): string[] {
    if (fstrzFlags == null) return []
    if (fasterizeHeaders[fstrzFlags] == null) return []
    const decodedFlags = fasterizeHeaders[fstrzFlags].split(',').map((flags: string) => flags.trim())
    return decodedFlags
  }

  isPlugged (server: string): boolean {
    return server === 'fasterize'
  }

  async generateCheckUrlResponse (body: GenerateCheckUrlBody): Promise<CheckUrlResponse> {
    try {
      const { url } = body
      if (!isValidURL(url)) throw new AppError(403, 'invalid url provided !')
      const { headers, status } = await this.getHeaders(url)
      const plugged = this.isPlugged(headers.server)
      if (!plugged) {
        return {
          plugged: plugged,
          statusCode: status
        }
      }
      return {
        plugged: plugged,
        statusCode: status,
        fstrzFlags: this.getFasterizeHeadersMeaning(headers['x-fstrz'] != null ? headers['x-fstrz'] : ''),
        cloudfrontStatus: this.getCFStatus(headers['x-cache'] != null ? headers['x-cache'] : ''),
        cloudfrontPOP: await this.getCFLocation(headers['x-amz-cf-pop'] != null ? headers['x-amz-cf-pop'] : '')
      }
    } catch (error) {
      throw new AppError(error.status, error.message)
    }
  }
}
