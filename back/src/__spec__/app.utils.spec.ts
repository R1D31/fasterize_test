import { isValidURL } from '../utils'

describe('test utils functions', () => {
  it('isValidUrl should return true', () => {
    const url = 'https://www.fasterize.com/fr/'
    const res = isValidURL(url)
    expect(res).toBe(true)
  })

  it('isValidUrl should return false', () => {
    const url = ''
    const res = isValidURL(url)
    expect(res).toBe(false)
  })

  it('isValidUrl should return false', () => {
    const url = 'google.com'
    const res = isValidURL(url)
    expect(res).toBe(false)
  })

  it('isValidUrl should return false', () => {
    const url = 'htps://www.fasterize.com/fr/'
    const res = isValidURL(url)
    expect(res).toBe(true)
  })
})
