import { Test, TestingModule } from '@nestjs/testing'
import { AppController } from '../app.controller'
import { AppService } from '../app.service'

describe('AppController tests', () => {
  let app: TestingModule
  let appControllers: AppController

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService]
    }).compile()
    appControllers = app.get<AppController>(AppController)
  })

  describe('generateCheckUrlResponse check', () => {
    it('should return plugged response', async () => {
      const url = 'https://www.fasterize.com/fr/'
      const expected = {
        plugged: true,
        statusCode: 200,
        fstrzFlags: ['optimisée', 'cachée'],
        cloudfrontStatus: 'MISS',
        cloudfrontPOP: 'Paris'
      }
      expect(await appControllers.generateCheckUrlResponse({ url })).toStrictEqual(expected)
    })

    it('should return non-plugged response', async () => {
      const url = 'https://www.google.com'
      const expected = {
        plugged: false,
        statusCode: 200
      }
      expect(await appControllers.generateCheckUrlResponse({ url })).toStrictEqual(expected)
    })

    it('should throw Error: invalid url provided !', async () => {
      const url = ''
      let message = ''
      try {
        await appControllers.generateCheckUrlResponse({ url })
      } catch (e) {
        message = e.message
      }
      expect(message).toBe('Error: invalid url provided !')
    })
  })
})
