import { AppService } from '../app.service'

describe('AppService', () => {
  let appService: AppService

  beforeAll(async () => {
    appService = new AppService()
  })

  describe('get headers service', () => {
    it('should return return headers', async () => {
      const url = 'https://www.minelli.fr/'
      const res = await appService.getHeaders(url)
      expect(res.headers).toHaveProperty('x-fstrz')
    })
  })

  describe('get cloudfront locations', () => {
    it('should return location', async () => {
      const url = 'MEL'
      const res = await appService.getCFLocation(url)
      expect(res).toBe('Melbourne')
    })

    it('should return return ""', async () => {
      const url = ''
      const res = await appService.getCFLocation(url)
      expect(res).toBe('')
    })
  })

  describe('get fstrzFlags', () => {
    it('should return "optimisée, cachée"', () => {
      const flags = 'o,c'
      const res = appService.getFasterizeHeadersMeaning(flags)
      expect(res).toStrictEqual(['optimisée', 'cachée'])
    })

    it('should return "optimisée, cachée"', () => {
      const flags = 'o,c'
      const res = appService.getFasterizeHeadersMeaning(flags)
      expect(res).toStrictEqual(['optimisée', 'cachée'])
    })

    it('should return ""', () => {
      const flags = ''
      const res = appService.getFasterizeHeadersMeaning(flags)
      expect(res).toStrictEqual([])
    })
  })

  describe('get isPlugged', () => {
    it('should return true', () => {
      const res = appService.isPlugged('fasterize')
      expect(res).toBe(true)
    })

    it('should return false', () => {
      const res = appService.isPlugged('aws')
      expect(res).toBe(false)
    })
  })

  describe('get CFStatus', () => {
    it('should return "MISS"', () => {
      const res = appService.getCFStatus('Miss from cloudfront')
      expect(res).toBe('MISS')
    })

    it('should return false', () => {
      const res = appService.getCFStatus('')
      expect(res).toBe('')
    })
  })
})
