import * as validURL from 'valid-url'

export const isValidURL = (url: string): boolean => {
  return validURL.isUri(url) !== undefined
}
