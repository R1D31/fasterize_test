import { Body, Controller, Post } from '@nestjs/common'
import { AppService } from './app.service'
import { CheckUrlResponse } from './domain/DTO/CheckUrlResponseDTO'
import { AppError } from './domain/exceptions/app.error'
import { GenerateCheckUrlBody } from './types/app'

@Controller()
export class AppController {
  constructor (private readonly appService: AppService) {}

  @Post()
  async generateCheckUrlResponse (@Body() body: GenerateCheckUrlBody): Promise<CheckUrlResponse> {
    try {
      return await this.appService.generateCheckUrlResponse(body)
    } catch (error) {
      throw new AppError(error.status, error.message)
    }
  }
}
