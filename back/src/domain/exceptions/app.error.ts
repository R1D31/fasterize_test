import { HttpException } from '@nestjs/common'

export class AppError extends HttpException {
  constructor (status: number, message: string) {
    super(message, status)
  }
}
