export interface CheckUrlResponse {
  plugged: boolean
  statusCode: number
  fstrzFlags?: string[]
  cloudfrontStatus?: string
  cloudfrontPOP?: string
}

export interface PluggedUrlResponse {
  plugged: true
  statusCode: number
  fstrzFlags: string[]
  cloudfrontStatus: string
  cloudfrontPOP: string
}

export interface NotPluggedUrlResponse {
  plugged: true
  statusCode: number
}
