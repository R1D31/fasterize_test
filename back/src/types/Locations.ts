export interface Locations {
  country: string
  city: string
  airport: string
}

export interface CFLocations {
  [key: string]: Locations
}

export interface CFData {
  self: string
  source: string
  nodes: CFLocations[]
}
