export interface ValidHeaders extends Headers {
  server: string
  'HTTP/2': number
  'x-fstrz'?: string
  'x-amz-cf-pop'?: string
  'x-cache'?: string
}

export interface GetHeadersResult {
  headers: ValidHeaders
  status: number
}
